const { readFile, writeFile } = require('fs/promises')

const readFileContents = async (filePath) => {
  try {
    const data = await readFile(filePath, 'utf8')
    return data
  } catch (err) {
    console.log(err)
  }
}

readFileContents('./chrome/manifest.json').then(async (data) => {
  console.log('Updating manifest')

  let manifest = JSON.parse(data)
  console.log('Current version', manifest.version)

  let version = parseFloat(manifest.version) + 0.1
  manifest.version = version.toPrecision(2)

  console.log(`Updating version: ${manifest.version}`)
  await writeFile('./chrome/manifest.json', JSON.stringify(manifest, null, 2))
})
// console.log(data);
// readFile("/chrome/manifest.json").then((data) => console.log(data));
// console.log("Update chrome extension version");
