const config = {
  baseURL: process.env.VUE_APP_API_URL || 'http://localhost:8000/'
  // baseURL: 'https://niswey.net/'
}

export default config
