// convert ms to 12hr format
export const getTimestamp = date => {
  let time = new Date(date * 1000)
  return time.toLocaleString('en-US', {
    hour: 'numeric',
    minute: 'numeric',
    hour12: true
  })
}

export const convertDate = date => {
  function join(t, a, s) {
    function format(m) {
      let f = new Intl.DateTimeFormat('en', m)
      return f.format(t)
    }
    return a.map(format).join(s)
  }

  let a = [{ day: '2-digit' }, { month: 'short' }, { year: 'numeric' }]
  return join(new Date(date), a, '-')
}

export const randomId = () =>
  Math.random()
    .toString(36)
    .replace(/[^a-z]+/g, '')
    .substr(2, 10)

export const isImageFile = file => {
  const IMAGE_TYPES = ['png', 'jpg', 'jpeg', 'webp', 'svg', 'gif']
  if (!file || !file.extension) return
  return IMAGE_TYPES.some(t => file.extension.toLowerCase().includes(t))
}
