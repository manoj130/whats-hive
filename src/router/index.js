import Vue from 'vue'
import VueRouter from 'vue-router'
import Activities from '../views/Activities.vue'
import Account from '../views/Account.vue'
import Labels from '../views/Labels.vue'
import MessageQueue from '../views/MessageQueue.vue'
import Templates from '../views/Templates.vue'
import Settings from '../views/Settings.vue'
import Lists from '../views/Lists.vue'
import Report from '../views/Report.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    alias: '/activities',
    name: 'Activities', // Don't change this
    component: Activities
  },
  {
    path: '/lists',
    name: 'Lists', // Don't change this
    component: Lists
  },
  {
    path: '/account',
    name: 'Account',
    component: Account
  },
  {
    path: '/templates',
    name: 'Templates',
    component: Templates
  },
  {
    path: '/labels',
    name: 'Labels',
    component: Labels
  },
  {
    path: '/message-queue',
    name: 'MessageQueue',
    component: MessageQueue
  },
  {
    path: '/settings',
    name: 'Settings',
    component: Settings
  },
  {
    path: '/report',
    name: 'Report',
    component: Report
  },
  {
    path: '/:catchAll(.*)*',
    redirect: { name: 'Activities' }
  }
]

const router = new VueRouter({
  // mode: 'history',
  base: process.env.BASE_URL,
  routes,
  linkExactActiveClass: 'active-link'
})

export default router
