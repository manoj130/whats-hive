import Vue from 'vue'
import Vuex from 'vuex'
// import axios from '@/utils/api.js';

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    userData: null,
    menuBarShow: false,
    loggedOut: false,
    errorApp: false,
    errorMsgApp: '',
    showBanner: false,
    showConflict: false,
    bannerContent: null,
    unreadCounts: {}
  },

  mutations: {
    setUserData: (state, data) => {
      state.userData = data
    },
    toggleMenuBar: state => {
      state.menuBarShow = !state.menuBarShow
    },
    logoutUser: state => {
      state.loggedOut = true
    },
    setErrorApp: (state, data) => {
      state.errorApp = data
    },
    setErrorMsgApp: (state, msg) => {
      state.errorMsgApp = msg
    },
    setBanner: (state, data) => {
      state.showBanner = data
    },
    setConflict: (state, data) => {
      state.showConflict = data
    },
    setBannerContent: (state, content) => {
      state.bannerContent = content
    },
    setUnreadCounts: (state, counts) => {
      state.unreadCounts = counts
    }
  },

  actions: {
    async logout(ctx, user_id) {
      //	TODO: Enable API
      console.log(user_id)
      // const { data } = await axios.get(`api/logout?user_id=${user_id}`);
      // if (data.status === 'ok') {
      ctx.commit('logoutUser')
      // } else {
      //   throw new Error();
      // }
    }
  }
})
