# Hubchat Chrome Extension (combined)

## Project setup

```
npm install
```

### Copy .env-example to .env

```
cp .env.example .env
```

### Compiles and hot-reloads for development

```
npm run dev
```

### Builds files for chrome extension (chrome folder) and changes manifest version

```
npm run build
```

### Lints and fixes files

```
npm run lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
